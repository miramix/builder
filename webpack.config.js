const path = require('path');
const { DefinePlugin, ProvidePlugin } = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextWebpackPlugin = require('extract-text-webpack-plugin');

module.exports = {
  mode: process.env.NODE_ENV || 'development',

  entry: {
    polyfill: 'babel-polyfill',
    app: path.join(__dirname, 'src/index.js'),
  },

  output: {
    filename: 'static/[name].bundle.js',
    path: path.join(__dirname, 'dist'),
    publicPath: '/',
  },

  module: {
    rules: [
      {
        test: /\.js/,
        exclude: path.join(__dirname, 'src/assets'),
        include: path.join(__dirname, 'src'),
        loader: 'babel-loader',
      },
      {
        test: /\.js/,
        include: path.join(__dirname, 'src/assets'),
        loader: 'script-loader',
      },
      {
        test: /\.css$/,
        use: ExtractTextWebpackPlugin.extract({
          fallback: 'style-loader',
          use: 'css-loader',
        }),
      },
      {
        exclude: [/\.js$/, /\.css$/, /\.json$/, /\.html$/],
        loader: 'file-loader',
        options: {
          name: 'static/media/[name].[hash:8].[ext]',
        },
      },
    ],
  },

  plugins: [
    new DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
    }),
    new ProvidePlugin({
      $: 'jquery',
      Popper: ['popper.js', 'default'],
      THREE: 'three',
    }),
    new CopyWebpackPlugin([
      { from: 'src/assets/', to: '.' },
    ]),
    new HtmlWebpackPlugin({
      template: path.join(__dirname, 'src/index.html'),
    }),
    new ExtractTextWebpackPlugin({
      filename: 'static/[name].bundle.css',
    }),
  ],

  resolve: {
    alias: {
      'three': path.join(__dirname, './vendor/threejs/three.js'),
    },
  },

  devtool: 'cheap-module-eval-source-map',

  devServer: {
    host: '127.0.0.1',
    port: 4200,
    contentBase: path.join(__dirname, 'dist'),
    clientLogLevel: 'none',
    historyApiFallback: true,
    stats: 'minimal',
  },
};
