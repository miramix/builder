import Renderer from './renderer';

import CubeShape from './shapes/cube';
import SphereShape from './shapes/sphere';
import DodecahedronShape from './shapes/dodecahedron';

const renderer = new Renderer('#root .viewport');

const shapes = {
  CubeShape,
  SphereShape,
  DodecahedronShape
};

let shape = null;

$('#root .menu button').draggable({
  revert: true,
  revertDuration: 0,
  cursor: 'move',
  helper: false,
  cancel: false,
  start(event) {
    renderer.controls.enabled = false;
    renderer.controls.update();

    const name = $(event.target).data('name');

    if (!shapes[name]) {
      return;
    }

    shape = new shapes[name](name, false);
    shape.addToScene(renderer.scene);
  },
  drag(event) {
    if (!shape) {
      return;
    }

    const intersect = renderer.checkMouseIntersection(event);

    if (!intersect) {
      return;
    }

    shape.opacity = 0.25;
    shape.visible = true;
    shape.moveTo(intersect.point);
  },
  stop() {
    if (!shape) {
      return;
    }

    shape.opacity = 1;
    shape.visible = true;

    renderer.controls.enabled = true;
    renderer.controls.update();
  },
});
