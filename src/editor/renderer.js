export default class Renderer {
  constructor(selector) {
    this.container = document.querySelector(selector);

    this.init();
    this.helpers();

    this.resize();
    this.animate();
  }

  init() {
    this.renderer = new THREE.WebGLRenderer({
      alpha: false,
      antialias: true,
    });
    this.renderer.setPixelRatio(window.devicePixelRatio);
    this.renderer.setSize(window.innerWidth, window.innerHeight);

    this.scene = new THREE.Scene();
    this.scene.background = new THREE.Color(0xe1e1e1);
    this.scene.name = 'Scene';

    const ambientLight = new THREE.AmbientLight(0xcccccc, 1);
    ambientLight.name = 'AmbientLight';
    this.scene.add(ambientLight);

    const directionalLight = new THREE.DirectionalLight(0xffffff, 0.4);
    directionalLight.name = 'DirectionalLight';
    directionalLight.position.set(1, 1, 1).multiplyScalar(10);
    this.scene.add(directionalLight);

    this.camera = new THREE.PerspectiveCamera(75, window.width / window.height, 1, 5000);
    this.camera.name = 'Camera';
    this.camera.position.set(5, 5, 5);
    this.scene.add(this.camera);

    this.controls = new THREE.OrbitControls(this.camera, this.renderer.domElement);

    this.raycaster = new THREE.Raycaster();

    this.container.appendChild(this.renderer.domElement);

    window.addEventListener('resize', () => this.resize(), false);
  }

  helpers() {
    this.floor = new THREE.Mesh(
      new THREE.PlaneGeometry(1000, 1000),
      new THREE.MeshBasicMaterial({ visible: false }),
    );
    this.floor.name = 'Floor';
    this.floor.rotation.x = -Math.PI / 2;
    this.scene.add(this.floor);

    this.axes = new THREE.AxesHelper(2);
    this.axes.name = 'Axes';
    this.scene.add(this.axes);

    this.grid = new THREE.GridHelper(10, 10, 0x0000ff, 0x808080);
    this.grid.name = 'Grid';
    this.grid.material.transparent = true;
    this.grid.material.opacity = 0.25;
    this.scene.add(this.grid);
  }

  resize() {
    const width = window.innerWidth;
    const height = window.innerHeight;

    this.camera.aspect = width / height;
    this.camera.updateProjectionMatrix();

    this.renderer.setSize(width, height);
  }

  animate() {
    window.requestAnimationFrame(() => this.animate());

    this.renderer.setPixelRatio(window.devicePixelRatio);
    this.renderer.render(this.scene, this.camera);
  }

  checkMouseIntersection(event) {
    const mouse = new THREE.Vector2(
      (event.clientX / window.innerWidth) * 2 - 1,
      -(event.clientY / window.innerHeight) * 2 + 1,
    );

    this.raycaster.setFromCamera(mouse, this.camera);

    const intersects = this.raycaster.intersectObject(this.floor);

    if (!intersects.length) {
      return;
    }

    return intersects[0];
  }
}
