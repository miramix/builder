export default class Shape {
  constructor(name, visible = true) {
    this.mesh = new THREE.Mesh();
    this.mesh.name = name;
    this.mesh.material = new THREE.MeshPhongMaterial({
      color: Math.random() * 0xffffff,
    });
    this.visible = visible;
  }

  get opacity() {
    return this.mesh.material.opacity;
  }

  set opacity(value) {
    this.mesh.material.transparent = value !== 1;
    this.mesh.material.opacity = value;
  }

  get visible() {
    return this.mesh.visible;
  }

  set visible(value) {
    this.mesh.visible = value;
  }

  moveTo(position) {
    this.mesh.position.copy(position);
  }

  addToScene(scene) {
    scene.add(this.mesh);
  }
}
